package org.creditsuisse;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;

/**
 * Test class for verifying the methods of {@link ShoppingCartService} class
 * 
 * @author Gaurav
 *
 */
public class ShoppingCartServiceTest {
	
	@InjectMocks
	ShoppingCartService service;
	
	@Rule
	public ExpectedException ex = ExpectedException.none();
	
	@Before
	public void setUp() throws Exception {
		initMocks(this);
	}

	@Test
	public void testForApples() throws IllegalArgumentException {
		String []arr = {"Apple","Apple"};
		assertEquals(70, service.getTotalPriceOfCart(Arrays.asList(arr)));
	}
	
	@Test
	public void testForBananas() throws IllegalArgumentException {
		String []arr = {"Banana","Banana","Banana"};
		assertEquals(60, service.getTotalPriceOfCart(Arrays.asList(arr)));
	}
	
	@Test
	public void testForMelons() throws IllegalArgumentException {
		String []arr = {"Melon","Melon","Melon","Melon","Melon"};
		assertEquals(150, service.getTotalPriceOfCart(Arrays.asList(arr)));
	}
	
	@Test
	public void testForLimes() throws IllegalArgumentException {
		String []arr = {"Lime","Lime","Lime","Lime","Lime"};
		assertEquals(60, service.getTotalPriceOfCart(Arrays.asList(arr)));
	}
	
	@Test
	public void testForMixedProducts() throws IllegalArgumentException {
		String []arr = {"Lime","Lime","Lime","Lime","Melon","Melon","Melon","Apple","Apple","Banana","Banana"};
		assertEquals(255, service.getTotalPriceOfCart(Arrays.asList(arr)));
	}
	
	@Test
	public void testForUnknownProducts() throws IllegalArgumentException {
		String []arr = {"Lime","Lime","Lime","Orange","Grape"};
		
		ex.expect(IllegalArgumentException.class);
		ex.expectMessage("Invalid items in Shopping Cart!!!");
		
		service.getTotalPriceOfCart(Arrays.asList(arr));
	}
}
