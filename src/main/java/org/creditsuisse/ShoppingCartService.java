package org.creditsuisse;

import static org.creditsuisse.ShoppingCartEnum.Apple;
import static org.creditsuisse.ShoppingCartEnum.Banana;
import static org.creditsuisse.ShoppingCartEnum.Lime;
import static org.creditsuisse.ShoppingCartEnum.Melon;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service class for processing the items present in the Shopping Cart
 * 
 * @author Gaurav
 *
 */
public class ShoppingCartService {

	private static final String EXCEPTION_MESSAGE = "Invalid items in Shopping Cart!!!";

	/**
	 * This method calculates the total price of items present in 
	 * the Shopping Cart; assuming that the valid items are present 
	 * in {@link ShoppingCartEnum} file. 
	 * 
	 * In case it encounters any invalid item, that particular item will just 
	 * be ignored and the total price of the remaining items will be returned.
	 * 
	 * 
	 * @param    list  						complete shopping cart items list
	 * @return   int						total price of valid items in 
	 * 										shopping cart
	 * 
	 * @throws   IllegalArgumentException	when an invalid item is found in 
	 * 										the Shopping Cart
	 */
	public int getTotalPriceOfCart(List<String> list) throws IllegalArgumentException{
		boolean matched = list.stream().anyMatch(item -> !ShoppingCartEnum.contains(item));
		if(matched) throw new IllegalArgumentException(EXCEPTION_MESSAGE); 
		
		Map<String, Integer> map = new HashMap<>();
		list.forEach(item -> {
			map.compute(item, (k, v) -> v == null ? 1 : v+1);
		});
		
		return calcApplePrice(map) 
				+ calcBananaPrice(map) 
				+ calcMelonPrice(map) 
				+ calcLimePrice(map);
	}

	private int calcBananaPrice(Map<String, Integer> map) {
		return getCount(map, Banana) * Banana.getPrice();
	}

	private int calcApplePrice(Map<String, Integer> map) {
		return getCount(map, Apple) * Apple.getPrice();
	}
	
	private int calcMelonPrice(Map<String, Integer> map) {
		int count = getCount(map, Melon);
		return ((count / 2) * Melon.getPrice()) + ((count % 2) * Melon.getPrice());
	}
	
	private int calcLimePrice(Map<String, Integer> map) {
		int count = getCount(map, Lime);
		return ((count / 3) * 2 * Lime.getPrice()) + ((count % 3) * Lime.getPrice());
	}

	private int getCount(Map<String, Integer> map, ShoppingCartEnum e) {
		return map.getOrDefault(e.getValue(),0);
	}
}
