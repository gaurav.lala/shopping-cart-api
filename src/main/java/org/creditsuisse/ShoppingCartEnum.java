package org.creditsuisse;

import java.util.Arrays;

/**
 * Enum class corresponding to the names of fruits
 * 
 * @author Gaurav
 *
 */
public enum ShoppingCartEnum {
	Banana("Banana",20),
	Apple("Apple",35),
	Melon("Melon",50),
	Lime("Lime",15);
	
	private String value;
	private int price;
	
	private ShoppingCartEnum(String value, int price){
		this.value = value;
		this.price = price;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public static boolean contains(String s) {
		return Arrays.asList(ShoppingCartEnum.values())
				.stream()
				.anyMatch(e -> e.value.equalsIgnoreCase(s));
	}
}
