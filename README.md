# Credit Suisse
Shopping Cart Service application 

## The problem
Implement a small service application that calculates the price of a basket of shopping.

Items are presented one at a time, in a list, identified by name - for example "Apple" or "Banana".
Multiple items are present multiple times in the list,
so for example ["Apple", "Apple", "Banana"] is a basket with two apples and one banana.

Items are priced as follows:
 - Apples are 35p each
 - Bananas are 20p each
 - Melons are 50p each, but are available as �buy one get one free�
 - Limes are 15p each, but are available in a �three for the price of two� offer

Given a list of shopping, calculate the total cost of those items.

## Assumptions & Approach
- ShoppingCartService class deals with the business logic
- ShoppingCartServiceTest is the test class created while following the TDD approach and testing ShoppingCartService class
- ShoppingCartEnum class used to store all the valid Shopping Cart items
- Items outside ShoppingCartEnum will be classified as invalid
- IllegalArgumentException thrown if invalid items are present in the shopping cart list
- Only business logic is implemented; the API is not fully functional for handling HTTP requests

## Requirements
- Java 1.8 and above
- JUnit 4.12
- Mockito 1.9.5

## Junit 
- TDD implemented